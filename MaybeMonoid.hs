module MaybeMonoid where

import Control.Monad
import Data.Monoid
import Test.QuickCheck

data Optional a =
  Nada | Only a
  deriving (Eq, Show)

instance Monoid a => Monoid (Optional a) where
  mempty = Nada
  mappend     Nada       b  = b
  mappend        a    Nada  = a
  mappend (Only a) (Only b) = Only (mappend a b)

newtype First' a =
  First' {getFirst' :: Optional a}
  deriving (Eq, Show)

instance Monoid (First' a) where
  mempty = First' Nada
  mappend  (First' Nada)    b  = b
  mappend  a    (First' Nada)  = a
  mappend  a b = a

firstMappend :: First' a -> First' a -> First' a
firstMappend = mappend

type FirstMappend =
     First' String
  -> First' String
  -> First' String
  -> Bool

asc :: Eq a => (a -> a -> a) -> a -> a -> a -> Bool
asc (<>) a b c = a <> (b <> c) == ((a <> b) <> c)

--instance Arbitrary (First' String -> Bool) where
--  arbitrary = frequency [
-- monoidLeftIdentity :: (Eq m, Monoid m) => m -> m -> m -> Bool
-- monoidAssoc a b c = (a <> (b <> c)) == ((a <> b) <> c)
--
-- -- monoidLeftIdentity :: (Eq m, Monoid m) => m -> Bool
-- monoidLeftIdentity a = (a <> mempty) == a
--
-- -- monoidRightIdentity :: (Eq m, Monoid m) => m -> Bool
-- monoidRightIdentity a = (mempty <> a) == a
--
-- main :: IO ()
-- main = do
--   quickCheck (monoidAssoc :: FirstMappend)
--   quickCheck (monoidLeftIdentity :: First' String -> Bool)
--   quickCheck (monoidRightIdentity :: First' String -> Bool)
--
