{-# LANGUAGE TypeOperators #-}
module MadLibs where

import Data.Monoid

type Verb = String
type Adjective = String
type Adverb = String
type Noun = String
type Exclamation = String

madlibbin' :: Exclamation -> Adverb -> Noun -> Adjective -> String
madlibbin' e adv noun adj = e <> "! he said " <>
                            adv <> " as he Xed " <>
                            noun <> " and Yed with the " <> adj <> " fellow"
madCat e adv noun adj = mconcat [
  e, "! he said ", adv, " as he Xed ",
  noun, " and Yed with the ", adj, " fellow" ]


