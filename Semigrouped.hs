module Semigrouped where
import Data.Semigroup
import Test.QuickCheck

newtype Identity a = Identity a deriving (Eq, Show)

instance Semigroup (Identity a) where
  a <> b = Identity (a <> b)

instance Arbitrary (Identity a) where
  arbitrary = return Identity

semigroupAssoc :: (Eq m, Semigroup m) => m -> m -> m -> Bool
semigroupAssoc a b c = (a<>(b<>c))==((a<>b)<>c)

type TrivialAssoc = Identity -> Identity -> Identity -> Bool

main :: IO ()
main = quickCheck (semigroupAssoc :: TrivialAssoc)

