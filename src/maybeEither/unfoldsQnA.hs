module UnfoldsQnA where
{- while folds are _catamorphisms_, unfolds are their dual: _anamorphisms_ -}

myIterate :: (a -> a) -> a -> [a]
myIterate f a = [f a] ++ (myIterate f (f a))

myUnfoldr :: (b -> Maybe (a, b)) -> b -> [a]
myUnfoldr f b = go f (f b)
  where go f       Nothing = []
        go f (Just (a, b)) = [a] ++ go f (f b)

betterIterate :: (a -> a) -> a -> [a]
betterIterate f seed = myUnfoldr (\b -> Just (b, f b)) seed

