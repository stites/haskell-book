module BinaryTreeQnA where

data BinaryTree a =
    Leaf
  | Node (BinaryTree a) a (BinaryTree a)
  deriving (Eq, Ord, Show)

insert' :: Ord a => a -> BinaryTree a -> BinaryTree a
insert' b Leaf = Node Leaf b Leaf
insert' b (Node left a right)
  | b == a = Node left a right
  | b <  a = Node (insert' b left) a right
  | b >  a = Node left a (insert' b right)

unfold :: Ord b => (a -> Maybe (a,b,a)) -> a -> BinaryTree b
unfold f a = go f (f a)
  where go f               Nothing = Leaf
        go f (Just (l1, b, l2)) = Node (go f (f l1)) b (go f (f l2))

treeBuild :: Integer -> BinaryTree Integer
treeBuild n = unfold terminator 0
  where terminator a = if n-a <= 0 then Nothing else Just (a+1, a, a+1)


