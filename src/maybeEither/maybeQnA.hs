module MaybeQnA where

import Data.Maybe hiding (isJust, isNothing, maybe, catMaybes)
import BinaryTreeQnA

import Prelude    hiding (maybe)
import Control.Arrow


vowels = "aeiou" :: String

maybeIf :: (a -> Bool) -> a -> Maybe a
maybeIf cond a = if cond a then Just a else Nothing

notThe :: String -> Maybe String
notThe = maybeIf ((/=) "the")

isVowel :: Char -> Maybe Char
isVowel = maybeIf ((flip elem) vowels)

-- we don't need to worry about safe heads since we use words
theFollowedByVowel :: (String, String) -> Bool
theFollowedByVowel (w1, w2) = (isNothing.notThe) w1 && (isJust.isVowel.head) w2

replaceThe :: String -> String
replaceThe str = unwords $ (maybe "a" id) <$> notThe <$> words str
replaceThe'    = unwords . map (maybe "a" id) . map notThe . words

countTheBeforeVowel :: String -> Integer
countTheBeforeVowel str = go ws 0
  where pairs = zip <*> tail
        ws    = pairs (words str)
        -- trying it out with recursion
        go     [] count = count
        go (p:ws) count = go ws (if theFollowedByVowel p then count+1 else count)

returnVowels :: String -> String
returnVowels str = catMaybes $ isVowel <$> str

countVowels :: String -> Int
countVowels = length . returnVowels

newtype Word' = Word' String deriving (Eq, Show)

notVowel :: Char -> Maybe Char
notVowel = maybeIf ((flip notElem) vowels)

countConsonants :: String -> Int
countConsonants = length . (catMaybes.(map notVowel))


mkWord :: String -> Maybe Word'
mkWord str = Word' <$> maybeIf isValid str
  where isValid str = let (nCs, nVs) = (countCs &&& countVs) str in nCs > nVs
        countCs     = countConsonants
        countVs     = countVowels

-- ==================================================================

data Nat = Zero | Succ Nat
  deriving (Eq, Show)

left = mkWord
natToInteger :: Nat -> Integer
natToInteger n = go n 0
  -- for tail recursion
  where go     Zero count = count
        go (Succ n) count = go n (count + 1)

-- for readability
integerToNat :: Integer -> Maybe Nat
integerToNat i | i  < 0 = Nothing
integerToNat i | i == 0 = Just Zero
integerToNat i         = Succ <$> integerToNat (i-1)

-- ==================================

isJust :: Maybe a -> Bool
isJust (Just _) = True
isJust _        = False

isNothing :: Maybe a -> Bool
isNothing = not.isJust

maybe :: b -> (a->b) -> Maybe a -> b
maybe default' _  Nothing = default'
maybe    _  func (Just a) = func a

fromMaybe :: a -> Maybe a -> a
fromMaybe a  Nothing = a
fromMaybe _ (Just a) = a

listToMaybe :: [a] -> Maybe a
listToMaybe [] = Nothing
listToMaybe  a = Just (head a)

maybeToList :: Maybe a -> [a]
maybeToList  Nothing = []
maybeToList (Just a) = [a]

catMaybes :: [Maybe a] -> [a]
catMaybes ms = go ms []
  where addToMemo m memo = if (isNothing m) then memo else (fromJust m):memo
        go     [] memo = memo
        go (m:ms) memo = go ms (addToMemo m memo)

-- reminder that foldr is what you want for infinite lists!
flipMaybe :: [Maybe a] -> Maybe [a]
flipMaybe = foldr collect (Just [])
  where concatJust    = (:).fromJust
        collect m acc = if isNothing m
                        then Nothing
                        else (Just (concatJust m)) <*> acc


