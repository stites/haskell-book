module EitherQnA where

lefts' :: [Either a b] -> [a]
lefts' = foldr merge'em []
  where merge'em  (Left a) memo = a:memo
        merge'em (Right _) memo =   memo

rights' :: [Either a b] -> [b]
rights' = foldr merge'em []
  where merge'em  (Left _) memo =   memo
        merge'em (Right a) memo = a:memo

partitionEithers :: [Either a b] -> ([a], [b])
partitionEithers = foldr merge'em ([],[])
  where merge'em  (Left a) (as, bs) = (a:as,   bs)
        merge'em (Right b) (as, bs) = (  as, b:bs)

eitherMaybe' :: (b -> c) -> Either a b -> Maybe c
eitherMaybe' f (Right b) = Just (f b)
eitherMaybe' _ _ = Nothing

either' :: (a->c) -> (b->c) -> Either a b -> c
either' f g  (Left a) = f a
either' f g (Right b) = g b

eitherMaybe'' :: (b -> c) -> Either a b -> Maybe c
eitherMaybe'' f = either' (const Nothing) (toJust.f)
  where toJust a = Just a

